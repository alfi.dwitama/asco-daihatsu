<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_auth extends CI_Model {

	function getUserByLogin($uname, $pass) { 
		$this->db->select("	pengguna.ID AS ID,
							pengguna.Nama_pengguna AS NAMA_PENGGUNA,
							pengguna.Username AS USERNAME,
							pengguna.Email AS EMAIL,
							pengguna.Password AS PASS,
							pengguna.No_hp AS NO_HP,
							pengguna.NIP AS NIP,
							mst_roles.ROLE AS ROLE,
							mst_roles.KD_ROLE AS KD_ROLE,
							pengguna.Puskesmas AS PUSKESMAS_USER,
							mst_puskesmas_new.NAMA_PUSKESMAS AS NAMA_PUSKESMAS
							");
	    $this->db->from('pengguna'); 
	    $this->db->join('mst_roles', 'pengguna.Role = mst_roles.KD_ROLE'); 
	    $this->db->join('mst_puskesmas_new', 'pengguna.Puskesmas = mst_puskesmas_new.KD_PUSKESMAS');      
	    $this->db->where('pengguna.Username', $uname);
	    $result = $this->getUsers($pass);

	    if (!empty($result)) {
	        return $result;
	    } else {
	        return null;
	    }
	}

	function getUsers($pass) {
	    $query = $this->db->get();
	    $result = $query->row();
	    if ($query->num_rows() > 0) {
	        $result = $query->row();
	        if (password_verify($pass, $result->PASS)) {
	            //We're good
	            return $result;
	        } else {
	            //Wrong password
	            return array();
	        }

	    } else {
	        return array();
	    }
	}

	// function getNamaPuskesmas($KD_PUSKESMAS)
	// {
	// 	$this->db->select("NAMA_PUSKESMAS");
	// 	$this->db->from('mst_puskesmas_new'); 
	// 	$this->db->where('KD_PUSKESMAS', $KD_PUSKESMAS);
	// 	$query = $this->db->get();
	// 	$result = $query->row();
	// 	return $result;
	// }

}