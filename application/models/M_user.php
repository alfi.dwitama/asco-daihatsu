<?php
class M_user extends CI_Model {

  	var $v_User 		= 'user';
  	var $column_order 		= array('NAMA', 'username');
	var $column_search 		= array('NAMA', 'username');
	var $order 				= array('ID' => 'desc');

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	private function _get_datatables_query()
	{
		$this->db->select("ID,NAMA, username", false );

		//add custom filter here
		if($this->input->post('keyword') and $_POST['search']['value']){
			$this->db->where($this->input->post('keyword').' like', '%'.$_POST['search']['value'].'%');
		}

		if(!empty($this->input->post('usergabung'))){
			$this->db->where('ID <>', $this->input->post('usergabung'));
		}
		
		// $this->db->where ('JENIS_DATA IS NULL', NULL);
		// $this->db->where ('FLAG_L =', '1');		
		// $this->db->or_where ('FLAG_L =', '3');
		
		$this->db->from($this->v_User);
		$i = 0;
		foreach ($this->column_search as $item){ // loop column 
			if($_POST['search']['value']){ // if datatable send POST for search
				if($i===0){ // first loop
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}
				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		if(isset($_POST['order'])){ // here order processing
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order)){
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->_get_datatables_query();
		return $this->db->count_all_results();
	}

	  function create_User($data)
    {  
        $query = $this->db->insert('User', $data);

        if ($query === FALSE) {
            log_message('error', $this->db->_error_message());
            return FALSE;
        }
        var_dump($query);
        return $query;
    }
    public function updateUser($where, $data)
    {
		$query = $this->db->where("ID = '$where'");
		$query = $this->db->update('User',$data);
        if ($query === FALSE) {
            log_message('error', $this->db->_error_message());
            return FALSE;
        }
        // var_dump($query);
        return $query;
    }

    function getUser($id){
    	$this->db->select('ID,NAMA,username,password');
    	$this->db->from('User');
    	$this->db->where('ID',$id);
    	$query = $this->db->get();
    	return $query->row();
    }

    public function getDetailViewers($id)
    {
    	$this->db->select('jenis_viewers, lokasi_viewers');
    	$this->db->from('User');
    	$this->db->where('ID',$id);
    	$query = $this->db->get();
    	return $query->row();
    }
    
    
}
