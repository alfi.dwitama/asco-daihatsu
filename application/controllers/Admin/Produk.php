<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
    {
        parent::__construct();

        $this->load->model('M_produk');
    }
	public function index()
	{
		$this->load->view('Admin/static/navbar_view.php');
		$this->load->view('Admin/static/sidebar_view.php');
		$this->load->view('Admin/produk/daftar_produk.php');
		
	}
	public function ajax_produk(){
		$list = $this->M_produk->get_datatables();	
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $r) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $r->NAMA;
			$row[] = $r->username;
			$row[] = '<div class="text-center">
								<div class="row">
								<a href="'.base_url().'Admin/Produk/edit/'.$r->ID.'" class="btn btn-sm btn-primary" style="margin-right:4px;"><i class="fa fa-edit""></i></a>
								<a href="#" class="btn btn-sm btn-primary" onclick="modalHapusPengguna('."'".$r->ID."'".');" style="margin-right:4px;"><i class="fa fa-trash"></i></a>
								</div>
						<div>';
			
		
			$data[] = $row;
		}
		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->M_produk->count_all(),
						"recordsFiltered" => $this->M_produk->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function tambah()
	{
		$this->load->view('Admin/static/navbar_view.php');
		$this->load->view('Admin/static/sidebar_view.php');
		$this->load->view('Admin/produk/tambah_produk.php');
	}
	public function simpan(){
		$nama			= $_POST['nama'];
		$username		= $_POST['username'];
		$password		= $_POST['password'];
		$hashed_password = password_hash($password, PASSWORD_BCRYPT);
		$data = array(
			'NAMA' => $nama,
			'username'		=> $username,
			'password'		=> $hashed_password,
		);
		$dataPengguna = $this->M_produk->create_Produk($data);
		if($dataPengguna == 1){
			$this->session->set_flashdata('message1', '
			<div class="alert alert-success alert-has-icon alert-dismissible fadeIn animated" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
              <div class="alert-body">
                <div class="alert-title">Berhasil</div>
                Pengguna baru berhasil tersimpan.
              </div>
            </div>
			');
			redirect(base_url().'Admin/Produk');
		}else{
		echo 'Simpan Data Kunjungan Gagal';
			$this->session->set_flashdata('message1', '
			<div class="alert alert-danger alert-has-icon alert-dismissible fadeIn animated" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
              <div class="alert-body">
                <div class="alert-title">Gagal</div>
                Pengguna baru gagal ditambahkan.
              </div>
            </div>
			');
			redirect(base_url().'Admin/Produk');
		}

	}
	public function hapus(){
		$id = $this->input->post('ID');

		$this->db->where('ID',$id);
		$this->db->delete('produk');
		$this->session->set_flashdata('message1', '
			<div class="alert alert-success alert-has-icon alert-dismissible fadeIn animated" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
              <div class="alert-body">
                <div class="alert-title">Berhasil</div>
                Data Pengguna berhasil dihapus.
              </div>
            </div>
			');
	}
	public function edit($param){
		$id = $param;
		$data['pengguna'] = $this->M_produk->getProduk($id);

		$this->load->view('Admin/static/navbar_view.php');
		$this->load->view('Admin/static/sidebar_view.php');
		$this->load->view('Admin/produk/edit_produk.php',$data);
	}
	public function update(){

		$nama			= $_POST['nama'];
		$username		= $_POST['username'];
		if(!empty($_POST['password'])){
		$password		= $_POST['password'];
		$hashed_password = password_hash($password, PASSWORD_BCRYPT);
		};
		$id 			= $_POST['id'];
		
		if($password != NULL){
			$data = array(
			'NAMA' 			=> $nama,
			'username'		=> $username,
			'password'		=> $hashed_password,
		);
		}else{
			$data = array(
			'NAMA' 			=> $nama,
			'username'		=> $username,
			
		);
		};
		$dataPengguna = $this->M_produk->updateProduk($id,$data);
		if($dataPengguna == 1){
			$this->session->set_flashdata('message1', '
			<div class="alert alert-success alert-has-icon alert-dismissible fadeIn animated" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
              <div class="alert-body">
                <div class="alert-title">Berhasil</div>
                Pengguna baru berhasil tersimpan.
              </div>
            </div>
			');
			redirect(base_url().'Admin/Produk');
		}else{
		echo 'Simpan Data Kunjungan Gagal';
			$this->session->set_flashdata('message1', '
			<div class="alert alert-danger alert-has-icon alert-dismissible fadeIn animated" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
              <div class="alert-body">
                <div class="alert-title">Gagal</div>
                Pengguna baru gagal ditambahkan.
              </div>
            </div>
			');
			redirect(base_url().'Admin/Produk');
		}

	}

	
}
