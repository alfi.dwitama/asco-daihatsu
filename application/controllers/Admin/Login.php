<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
    {
        parent::__construct();

        $this->load->model('M_auth');
    }
	public function index()
	{
		$this->load->view('Admin/login_view.php');
		
	}

	public function do_login()
	{

		$uname  	= $this->input->post('uname');
		$pass 		= $this->input->post('pass');

        if (isset($uname) && isset($pass)) {
            $user = $this->M_auth->getUserByLogin($uname, $pass);
            $passHash = strval($user->PASS);
            if ($user) {
            	$data_session = array(
					'ID' 				=> $user->ID,
					'USERNAME' 			=> $user->USERNAME,
					'status_login' 		=> "loginactive"
				);
				$this->session->set_userdata($data_session);
		    }else{
            	$this->session->set_flashdata('pesan2', '
				<div class="alert alert-warning alert-dismissible show fadeIn animated">
                  <div class="alert-body">
                    <button class="close" data-dismiss="alert">
                      <span>&times;</span>
                    </button>
                    <strong>Login Gagal</strong><br>Periksa kembali username dan password. 
                  </div>
                </div>');
            	redirect(base_url().'login');
            }
        }
	}
	
	public function do_logout()
	{
		$data_session = array(
							'ID' 				=> '',
							'NAMA_PENGGUNA' 	=> '',
							'USERNAME' 			=> '',
							'EMAIL' 			=> '',
							'NO_HP' 			=> '',
							'NIP' 				=> '',
							'PUSKESMAS_USER' 	=> '',
							'role' 				=> '',
							'NAMA_ROLE' 		=> '',
							'PUSKESMAS_ACTIVE' 	=> '',
							'NAMA_PUSKESMAS' 	=> '',
							'status_login' 		=> ''
				);
		$this->session->unset_userdata($data_session);
    	$this->session->sess_destroy();

	    redirect(base_url().'login');
	}
}
