<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title  -->
    <title>Selamat Datang di Website Asco Daihatsu Lumajang</title>

    <!-- Favicon  -->
    <link rel="icon" href="<?php echo base_url();?>/assets/img/mobil/logo transparan.png">

    <!-- Style CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>/assets/style.css">

</head>

<body>
    <!-- Preloader Start -->
    <div id="preloader">
        <div class="preload-content">
            <div id="world-load"></div>
        </div>
    </div>
    <!-- Preloader End -->

    <!-- ***** Header Area Start ***** -->
    <header class="header-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <nav class="navbar navbar-expand-lg">
                        <!-- Logo -->
                        <a class="navbar-brand" href="index.html"><img src="<?php echo base_url();?>/assets/img/mobil/unnamed.jpg" alt="Logo"style="height: 100px;"></a>
                        <!-- Navbar Toggler -->
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#worldNav" aria-controls="worldNav" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                        <!-- Navbar -->
                        <div class="collapse navbar-collapse" id="worldNav">
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item active">
                                    <a class="nav-link" href="<?php echo base_url();?>">Beranda <span class="sr-only">(current)</span></a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Produk</a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="index.html">Visi & Misi</a>
                                        <a class="dropdown-item" href="catagory.html">Struktur Organisasi</a>
                                        <a class="dropdown-item" href="<?php echo base_url();?>index.php/Sejarah">Sejarah Kota Batu</a>
                                        <a class="dropdown-item" href="regular-page.html">Lambang Kota Batu</a>
                                        <a class="dropdown-item" href="contact.html">Geografis Kota Batu</a>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Promo Daihatsu</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Simulasi Kredit</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Hubungi Kami</a>
                                </li>
                            </ul>
                            <!-- Search Form  -->
                            <div id="search-wrapper">
                                <form action="#">
                                    <input type="text" id="search" placeholder="Search something...">
                                    <div id="close-icon"></div>
                                    <input class="d-none" type="submit" value="">
                                </form>
                            </div>
                        </div>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- ***** Header Area End ***** -->