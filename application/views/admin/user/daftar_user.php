<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<h3 class="page-title">Daftar Pengguna</h3>
					<div class="row">
						<div class="col-md-12">

							<!-- TABLE STRIPED -->
							<div class="panel">
								<div class="panel-heading">
									
								</div>
								<div class="panel-body">
									<a href="<?php echo base_url();?>Admin/User/Tambah"><button type="button" class="btn btn-primary" >Tambah Pengguna</button></a>
									
									<table class="table table-striped" id="datatabel_pasien">
										<thead>
											<tr>
												<th>#</th>
												 <th>Nama</th>
                           						 <th>Username</th>
                            					<th>Aksi</th>
                            				</tr>
										</thead>
										<tbody>
											
										</tbody>
									</table>
								</div>
							</div>
							<!-- END TABLE STRIPED -->
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Modal Hapus Pengguna-->
      <div class="modal fade" id="modal_hapus" role="dialog">
          <div class="modal-dialog modal-sm">
              <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <div class="row">
                      <div class="col-md-12">
                          <center>
                              Apakah Anda ingin <br>
                              menghapus pengguna? <br><br>

                              <form>
                                
                                <input type="text" name="ID" id="ID">
                                <a style="color : white;" data-dismiss="modal" class="btn btn-secondary">Tidak</a>
                                <a style="margin-left: 15px; color : white;" onclick="hapusPengguna();" class="btn btn-primary">Hapus Pengguna</a>
                              </form>

                          </center>
                      </div>
                    </div>
                  </div>
              </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
      </div>
      <!-- End modal -->
		<!-- Javascript -->
	<script src="<?php echo base_url()?>assets/admin/vendor/jquery/jquery.min.js"></script>
	<script src="<?php echo base_url()?>assets/admin/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url()?>assets/admin/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="<?php echo base_url()?>assets/admin/scripts/klorofil-common.js"></script>

	<script src="<?php echo base_url()?>assets/modules/datatables/datatables.min.js"></script>
  <script src="<?php echo base_url()?>assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
  <script src="<?php echo base_url()?>assets/modules/datatables/Select-1.2.4/js/dataTables.select.min.js"></script>
  <script src="<?php echo base_url()?>assets/modules/jquery-ui/jquery-ui.min.js"></script>
  <script src="<?php echo base_url()?>assets/modules/select2/dist/js/select2.full.min.js"></script>
  <script src="<?php echo base_url()?>assets/modules/jquery-selectric/jquery.selectric.min.js"></script>
  <script src="<?php echo base_url()?>assets/js/jquery-ui.min.js"></script>
  <script src="<?php echo base_url()?>assets/modules/cleave-js/dist/cleave.min.js"></script>
  <script src="<?php echo base_url()?>assets/modules/cleave-js/dist/addons/cleave-phone.us.js"></script>
		<!-- Script Data Tabel -->
      <script type="text/javascript">
        var tabel
        var selText
        var selVal
        // var d = new Date();
        // var strDate = d.getDate() + "/" + (d.getMonth()+1) + "/" + d.getFullYear();

        $(document).ready(function() {

          table = create_table(null);
          $(".dropdown-menu a").click(function(){
            selVal = $(this).attr('data-id');
            selText = $(this).text();
            $(this).parents('.dropdown').find('.dropdown-toggle').html('<i class="fas fa-filter"></i> '+selText+' <span class="caret"></span>');
            table.api().destroy();
            table.api().ajax.reload();
            table = create_table(selVal);
          });

        });

        function create_table(params){
          return $('#datatabel_pasien').dataTable( {
              "processing": true,
              "serverSide": true,
              "ordering": true, // Set true agar bisa di sorting
              "ajax":
              {
                  "url": "<?php echo base_url('Admin/User/ajax_user') ?>", 
                  "type": "POST",
                  "data" : {
                      // "cari" : "",
                      "keyword" : params
                  }
              },
              "aLengthMenu": [[5, 10, 50],[ 5, 10, 50]], // Combobox Limit
              "columnDefs": [
                  { 
                      "className": "text-center",
                      "searchable": false,
                      "orderable": false,
                      "targets": 0
                  },
                  { 
                      "searchable": false,
                      "orderable": false,
                      "targets": -1
                  },
                  { 
                      "searchable": false,
                      "orderable": false,
                      "targets": -2
                  },
              ],
              responsive: true,
            } );
        }

          function modalReset(id){
           $.ajax({
              url : "<?php echo site_url('pengguna/getPengguna')?>/" + id,
              type: "GET",
              dataType: "JSON",
              success: function(data)
              {
                 
                  $('#ID').text(data.ID);
                  $('#link').html('<a style="margin-left: 15px; color : white;" class="btn btn-primary btnreset" onclick="modalGenerate('+id+')">Reset Password</a>')
                  $('#modal_reset').modal('toggle'); 

              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                  alert('Error Pada Saat Mengambil Data');
              }
          });
        }

        function modalGenerate(id){
           $.ajax({
             url : "<?php echo site_url('Pengguna/resetPassword')?>/" +id,
              type: "GET",
              dataType: "JSON",
              success: function(data)
              {
                  
                  // $('#pass').val(data);
                  $('#password').val(data);
                  $('#pass').html('<h4 id="pass2">'+ data +'</h4>')
                  // $('#modal_generate').modal('show');
                  $.ajax({
                  url : "<?php echo site_url('Pengguna/getPengguna')?>/" +id,
                  type: "GET",
                  dataType: "JSON",
                  success: function(data)
                    {
                  
                  $('#header-generate').html('<h5 style="text-align: center;">Reset Password Akun '+ data.Nama_pengguna+ '</h5>  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>       </button>')
                  $('#modal_generate').modal('show');
                  
                },
                error: function (jqXHR, textStatus, errorThrown)
              {
                  alert('Error Pada Saat Mengambil Data');
              }
          });
                },
                error: function (jqXHR, textStatus, errorThrown)
              {
                  alert('Error Pada Saat Mengambil Data');
              }
          });
          
        }
         $('.btnreset').on("click",function() {
          $('$modal_reset').modal('toggle');
          });

        $('.btngen').on("click",function() {
          $('#modal_generate').modal('toggle');
          $('$modal_reset').modal('toggle');
          });

        function modalHapusPengguna(kd_pengguna){
         
          $("#ID").val(kd_pengguna);
          $("#modal_hapus").modal("toggle");
        }
         function hapusPengguna(){
          var kd_pengguna = $("#ID").val();
          $.ajax({ 
              "url": "<?php echo base_url('Admin/User/hapus') ?>", 
              "type": "POST",
              "data" : {
                "ID" : kd_pengguna,
                
              },
              "success" : function(data){
                  location.reload();
                 }
          });

        }

        
         function salin() {
      var salin = document.getElementById("pass2");
      var textArea = document.createElement("textarea");
      textArea.value = salin.textContent;
      document.body.appendChild(textArea);
      textArea.select();
      document.execCommand("Copy");
      alert("Password Berhasil di Salin " + textArea.value);
      textArea.remove();
       
    }
      </script>
      <!-- End Script Data Tabel -->